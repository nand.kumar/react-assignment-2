import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  useParams,
} from "react-router-dom";
import Home from './components/Home';
import Blog from './components/Blog';
import Edit from './components/Edit';
import Header from './components/Header';
import Navbar from './components/Navbar';
import About from './components/About';
import Contact from './components/Contact';

function App() {

  return (
    <BrowserRouter>
    <Navbar/>
    <Header/>
    <Routes>
      <Route exact path="/" element={<Home />}/>
      <Route exact path="/home" element={<Home />}/>
      <Route exact path="/blog/:id" element={<Blog />} />
      <Route exact path="/blog/edit/:id" element={<Edit />} />
      <Route exact path="/about" element={<About />}/>

      <Route exact path="/contact" element={<Contact />}/>


    </Routes>
  </BrowserRouter>
  );
}

export default App;
