import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

const Home = () => {
  const [blogs, setBlogs] = useState([]);
  const [reload, setReload] = useState(0);
  const navigate = useNavigate();
  useEffect(() => {
    const fetchData = async () => {
      const data = await axios.get("http://localhost:5000/blogs");
      // console.log(data.data);
      setBlogs(data.data);
    };
    fetchData();
  }, [reload]);

  const deleteBlog = async (id) => {
    const url = `http://localhost:5000/blogs/${id}`;
    await axios.delete(url);
    setReload(reload + 1);
    navigate("/");
  };

  return (
    <div className="blog-container">
      {blogs.map(
        (blog) => (
          <div className="blog" key={blog.id}>
            <h1>{blog.title}</h1>
            <p>{blog.description}</p>
            <small>{blog.authorName}</small>

            <div className="btn">

            <Link to={`/blog/${blog.id}`}>
              <button>Show</button>
            </Link>
            <Link to={`/blog/edit/${blog.id}`}>
              <button>Edit</button>
            </Link>
            <button onClick={() => deleteBlog(blog.id)}>Delete</button>
            </div>
          </div>
        )
        // <Blog blog={blog} key={blog.id}/>
      )}
    </div>
  );
};

export default Home;
