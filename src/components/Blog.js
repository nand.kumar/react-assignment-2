import axios from 'axios';
import React, {useState,useEffect} from 'react';
import { Link, useParams,useNavigate } from 'react-router-dom';

const Blog = () => {
    const {id}=useParams();
    const navigate=useNavigate();
    const [blog, setBlog] = useState({});
    const [reload, setReload] = useState(0);
    const fetchData=async()=>{
        const url=`http://localhost:5000/blogs/${id}`;
        const data=await axios.get(url);
        // console.log(data)
        setBlog(data.data);
    }
    useEffect(() => {
       fetchData();
    },[reload]);


    const deleteBlog=async()=>{
           const url=`http://localhost:5000/blogs/${id}`
           await axios.delete(url);
           setReload(reload+1);
           navigate('/');
    }
  return (
    <div className= "container">
        <h1>{blog.title}</h1>
        <p>{blog.description}</p>
        <small>{blog.authorName}</small>


        <div className="btn">
        <Link to={`/blog/edit/${blog.id}`} ><button>Edit</button></Link>
        <button onClick={deleteBlog}>Delete</button>
        </div>
    </div>
  )
}

export default Blog