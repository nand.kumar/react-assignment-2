import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import "../App.css";
import axios from "axios";

const Edit = () => {
  const navigate = useNavigate();
  const {id} = useParams();
  const [editTitle, setTitle] = useState("");
  const [editAuthorname, setAuthor] = useState("");
  const [editDescription, setDesc] = useState("");

  useEffect(() => {
    axios
      .get(`http://localhost:5000/blogs/${id}`)
      .then((res) => res.data)
      .then((data) => {
        setTitle(data.title);
        setAuthor(data.authorName);
        setDesc(data.description);
      });
  }, []);

  const handleUpdate = async () => {
    await axios.put(`http://localhost:5000/blogs/${id}`, {
      authorName: editAuthorname,
      description: editDescription,
      title: editTitle,
    });
    navigate("/");
  };
  return (
    <div className="container">
      <div className="text">

      <label htmlFor="title">Title:</label>
      <input
        name="title"
        type="text"
        value={editTitle}
        onChange={(e) => setTitle(e.target.value)}
        />
        </div>
      <div className="text">


      <label htmlFor="author">Author:</label>
      <input
        name="desc"
        type="text"
        value={editDescription}
        onChange={(e) => setDesc(e.target.value)}
        />
        </div>
      <div className="text">


      <label htmlFor="desc"> Description:</label>
      <input
        name="author"
        type="text"
        value={editAuthorname}
        onChange={(e) => setAuthor(e.target.value)}
        />
        </div>
      <div className="btn">

      <button onClick={handleUpdate}>Update</button>
      <button onClick={() => navigate("/")}>Cancel</button>
      </div>
    </div>
  );
};

export default Edit;
